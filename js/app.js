'use strict';
// Filename: app.js
define([
  'jquery',
  'underscore',
  'backbone',
  'collections/vehicles'
  // Request router.js
], function ($, _, Backbone, Vehicles) {
  var initialize = function () {
    Backbone.sync = function (method, model, options) {
      var xhr = new XMLHttpRequest();
      var serialize = function(obj, prefix) {
        var str = [];
        for(var p in obj) {
          var k = prefix ? prefix + "[" + p + "]" : p, v = obj[p];
          str.push(typeof v == "object" ?
            serialize(v, k) :
            encodeURIComponent(k) + "=" + encodeURIComponent(v));
        }
        return str.join("&");
      };
      xhr.open(method, model.url);
      xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status === 200 && options && options.success) {
          options.success(xhr.responseText);
        } else if (xhr.readyState == 4 && options && options.error) {
          options.error();
        }
      };

      if (method === "POST") {
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.send(serialize(model.form));
      }
      else
        xhr.send(null)
    };

    // Pass in our Router module and call it's initialize function
    var VehiclesCollection = new Vehicles();
    VehiclesCollection.initialize();
    setTimeout(function(){
      console.log(VehiclesCollection)
    },2000)
  };
  return {
    initialize: initialize
  };
});