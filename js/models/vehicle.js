'use strict';
define([
  'underscore',
  'backbone'],
  function (_, Backbone) {
    var Vehicle = Backbone.Model.extend({
      name: "",
      k: "",
      positions: {},
      initialize: function (params) {
        this.name = params.name;
        this.k = params.k;
        this.positions = {};
      },
      addPosition: function (timestamp, latLang) {

        this.positions[timestamp] = latLang;
      },
      getPositions: function(){
        return this.positions;
      }
    });

    return Vehicle;
  });
