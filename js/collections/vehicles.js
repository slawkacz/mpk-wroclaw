'use strict';
define([
  'underscore',
  'backbone',
  'config',
  'models/vehicle'],
  function (_, Backbone, config, Vehicle) {
    var Vehicles = Backbone.Collection.extend({
      model: Vehicle,
      url: "http://pasazer.mpk.wroc.pl/position.php",
      initialize: function () {
        var self = this;
        this.form = {busList: config.busList};
        this.sync();
        setInterval(this.sync.bind(this), 20000);
      },
      parse: function (models) {
        var self = this;
        models.forEach(function (model) {
          var veh = self.findWhere({name: model.name, k: model.k});
          if (veh) {

            veh.addPosition(new Date().getTime(), {x: model.x, y: model.y});
            //console.log(veh.getPositions())
          } else {
            var mod = new Vehicle({k: model.k, name: model.name});
            mod.positions[new Date().getTime()] = {x: model.x, y: model.y};
            self.add(mod);

          }
        });
      },
      sync: function () {
        var self = this;
        Backbone.sync("POST", self, {success: function (response) {
          self.parse(JSON.parse(response));
        }, error: function () {
        }});
      }

    });
    return Vehicles;
  });
