'use strict';
define([], function () {
  var Config = (function () {
    var busList = [];
    busList['bus'] = localStorage.getItem('watchedBuses') || ['d','a'];
    busList['tram'] = localStorage.getItem('watchedTrams') || ['17'];
    busList['train'] = localStorage.getItem('watchedTrains') || [];
    var busStops = localStorage.getItem('busStops') || [];
    return {
      busList: busList,
      busStops: busStops
    };
  })();
  return Config;
});